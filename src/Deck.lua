local BASE = utils.getBase(...)

local Deck = Class{}

function Deck:init(game, x, y, width, height)
  assert(game)
  self.game = game
  self.x = x
  self.y = y
  self.width = width
  self.height = height
  self.max_cards = 5
  self.cards = {}
end

function Deck:draw()
  love.graphics.setColor(1, 1, 1)
  love.graphics.rectangle("fill", self.x, self.y, self.width, self.height, 4, 4)
  for _, card in pairs(self.cards) do
    card:draw()
  end
end

function Deck:addCard(card)
  assert(card)
  assert(not card.sold)
  assert(#self.cards < self.max_cards)
  table.insert(self.cards, card)
  self:updateCardPositions()
end

function Deck:updateCardPositions()
  for i, card in ipairs(self.cards) do
    card:set(self.x+(self.width-self.width/self.max_cards*#self.cards)/2+self.width/self.max_cards*(i-1), self.y, self.width/self.max_cards, self.height)
  end
end

function Deck:testPoint(x, y)
  return x >= self.x and y >= self.y and x < self.x+self.width and y + self.y+self.height
end

function Deck:mousepressed(x, y, button)
  assert(self:testPoint(x, y))
  local card_i = math.floor((x - self.x - (self.width-self.width/self.max_cards*#self.cards)/2) / (self.width/self.max_cards))+1
  local card = self.cards[card_i]
  if button == 1 then
    if self.game.player_turn and self.game:verifyEnoughMoney(card.price) then
      self.game.buyFor(card.price)
      self.game.addToDeck(deepcopy(card))
      card.sold = true
    end
  elseif button == 2 then
    card:mousepressed(x, y, button)
  end
end

return Deck
