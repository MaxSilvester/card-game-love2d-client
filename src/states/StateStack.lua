local StateStack = Class{}

function StateStack:init()
    self.states = {}
end

function StateStack:update(dt)
    self.states[#self.states]:update(dt)
end

function StateStack:mousepressed(x, y, button)
    self.states[#self.states]:mousepressed(x, y, button)
end

function StateStack:keypressed(key)
    self.states[#self.states]:keypressed(key)
end

function StateStack:draw()
    for i, state in ipairs(self.states) do
        state:draw()
    end
end

function StateStack:clear()
    self.states = {}
end

function StateStack:push(state)
    table.insert(self.states, state)
    state:enter()
end

function StateStack:pop()
    self.states[#self.states]:exit()
    table.remove(self.states)
end

function StateStack:current()
  return self.states[#self.states]
end

return StateStack
