local BASE = utils.getBase(...)
local BASEBASE = utils.getBase(BASE)

local BaseState = require(BASEBASE..".BaseState")

local IAttackState = Class{__includes = BaseState}

function IAttackState:init(inGameState)
  assert(inGameState)
  self.inGameState = inGameState
end

function IAttackState:update(dt)
  self.inGameState:updateMessages()
end

function IAttackState:draw()
end

function IAttackState:mousepressed(x, y, button)
  if self.inGameState.myShop:testPoint(x, y) then
    self.inGameState.myShop:mousepressed(x, y, button)
  elseif self.inGameState.myDeck:testPoint(x, y) then
    self.inGameState.myDeck:mousepressed(x, y, button)
  end
end

function IAttackState:buyCard(card)
  assert(card)
  self.inGameState:addMyEvent("buyCard", deepcopy(card))
  self.inGameState.myDeck:addCard(card)
end

return IAttackState
