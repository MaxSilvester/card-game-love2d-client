local BASE = utils.getBase(...)
local BASEBASE = utils.getBase(BASE)

local BaseState = require(BASEBASE..".BaseState")

local CardInfoState = Class{__includes = BaseState}

function CardInfoState:init(card)
  assert(card)
  self.card = card
  self.width = gWidth
  self.height = gHeight
  self.simple_properties = {
    {"Name", self.card.full_name},
    {"Health", self.card.hp},
    {"Damage", self.card.dmg},
    --{"Price", self.card.price or 1},
    {"Rarity", self.card.weight}
  }
end

function CardInfoState:update(dt)
end

function CardInfoState:draw()
  love.graphics.setColor(0, 0, 0, .9)
  love.graphics.rectangle("fill", 0, 0, self.width, self.height)
  love.graphics.setFont(gFonts.big)
  love.graphics.setColor(1, 1, 1)
  for i, property in ipairs(self.simple_properties) do
    local key, value = unpack(property)
    love.graphics.printf(key..": "..value, self.width/4, self.height/10+(gFonts.big:getHeight()*1.5)*(i-1), self.width/2, "left")
  end
  love.graphics.printf("Description:", self.width/4, self.height/10+(gFonts.big:getHeight()*1.5)*#self.simple_properties, self.width/2, "left")
  local text = self.card.description
  if text == nil then
    text = "MISSING"
    love.graphics.setColor(1, 0, 0)
  end
  love.graphics.printf(text, self.width/4, self.height/10+(gFonts.big:getHeight()*1.5)*(#self.simple_properties+.7), self.width/2, "left")
end

function CardInfoState:keypressed(key)
  if key == "escape" then
    gStateStack:pop()
  end
end

return CardInfoState
