local BASE = utils.getBase(...)
local BASEBASE = utils.getBase(BASE)

local BaseState = require(BASEBASE..".BaseState")

local IDefendState = Class{__includes = BaseState}

function IDefendState:init(inGameState)
  assert(inGameState)
  self.inGameState = inGameState
end

function IDefendState:update(dt)
end

function IDefendState:draw()
  love.graphics.print("DEFEND", 0, 0)
end

function IDefendState:mousepressed(x, y, button)
  if self.inGameState.myShop:testPoint(x, y) then
    self.inGameState.myShop:mousepressed(x, y, button)
  elseif self.inGameState.myDeck:testPoint(x, y) then
    self.inGameState.myDeck:mousepressed(x, y, button)
  end
end

function IDefendState:buyCard(card)
  assert(card)
  self.inGameState:addMyEvent("buyCard", deepcopy(card))
  self.inGameState.myDeck:addCard(card)
end

return IDefendState
