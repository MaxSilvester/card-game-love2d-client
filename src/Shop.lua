local BASE = utils.getBase(...)

local Card = require(BASE..".Card")

local Shop = Class{}

local function pick_random_with_weights(elements)
  local total_weights = 0
  local weights = {}
  for _, element in pairs(elements) do
    if element.weight == nil then
      table.insert(weights, 1)
    else
      table.insert(weights, element.weight)
    end
  end
  for _, weight in ipairs(weights) do
    total_weights = total_weights + weight
  end
  local choice = love.math.random(total_weights)
  for i, weight in ipairs(weights) do
    choice = choice - weight
    if choice <= 0 then
      return elements[i]
    end
  end
end

function Shop:init(game, x, y, width, height)
  assert(game)
  self.game = game
  self.x = x
  self.y = y
  self.width = width
  self.height = height
  self.max_cards = 5
  local cards_json = json_decode_file("assets/cards.json")
  self.possible_cards = {}
  for _, card_table in ipairs(cards_json) do
    local card = Card(card_table)
    table.insert(self.possible_cards, card)
  end

  -- print(require("socket.http").request("http://0.0.0.0:8000/get_messages"))
  -- print(require("socket.http").request("http://0.0.0.0:8000/post_message", json_encode(self.possible_cards)))

  self.cards = {}
  self:refill()
end

function Shop:refill()
  for i = 1, self.max_cards do
    if true or self.cards[i] == nil or self.cards[i].sold then
      local card = deepcopy(pick_random_with_weights(self.possible_cards))
      --card:set(self.x+(self.width/self.max_cards)*(i-1), self.y, self.width/self.max_cards, self.height)
      card:set(self.x, self.y+self.height/self.max_cards*(i-1), self.width, self.height/self.max_cards)
      self.cards[i] = card
    end
  end
end

function Shop:draw()
  --require("socket.http").request("http://0.0.0.0:8000/get_messages")
  love.graphics.setColor(1, 1, 1)
  love.graphics.rectangle("fill", self.x, self.y, self.width, self.height, 4, 4)
  for _, card in pairs(self.cards) do
    card:draw()
  end
end

function Shop:testPoint(x, y)
  return x >= self.x and y >= self.y and x < self.x+self.width and y + self.y+self.height
end

function Shop:mousepressed(x, y, button)
  assert(self:testPoint(x, y))
  local card_i = math.floor((y - self.y) / (self.height/self.max_cards))+1
  local card = self.cards[card_i]
  if button == 1 then
    if true then
      --self.game.buyFor(card.price)
      gStateStack:current():buyCard(deepcopy(card))
      card.sold = true
    end
  elseif button == 2 then
    card:mousepressed(x, y, button)
  end
end

return Shop
