local BASE = utils.getBase(...)

local CardInfoState = require(BASE..".states.game.CardInfoState")

local valid_attributes = {
  elusive = {
    color = {1, 1, .8},
    name = "Elusive"
  },
  evolvable = {
    name = "Evolvable",
    description = "Can evolve after being placed for 10 rounds"
  },
  armored = {
    name = "Armored"
  },
  quick_attack = {
    name = "Quick Attack"
  }
}

local Card = Class{}

function Card:init(card_table)
  self.full_name = card_table.full_name
  self.attributes = {}
  for _, attribute in ipairs(card_table.attributes) do
    assert(valid_attributes[attribute] ~= nil, string.format("attribute %q not found!", attribute))
    table.insert(self.attributes, attribute)
  end
  self.hp = card_table.hp
  self.dmg = card_table.dmg
  self.price = card_table.price
  self.rarity = card_table.rarity
  self.weight = card_table.weight
  self.symbol = card_table.symbol
  self.sold = false
  self.description = card_table.description
end

function Card:set(x, y, width, height)
  self.x = x
  self.y = y
  self.width = width
  self.height = height
end

function Card:draw()
  -- Draw border and background

  love.graphics.setColor(0, 0, 0)
  love.graphics.rectangle("fill", self.x, self.y, self.width, self.height, 4, 4)

  if self.sold then
    love.graphics.setColor(1, 0, 0)
  else
    love.graphics.setColor(1, 1, 1)
  end
  love.graphics.rectangle("fill", self.x+1, self.y+1, self.width-2, self.height-2, 4, 4)

  love.graphics.setColor(0, 0, 0)
  love.graphics.rectangle("fill", self.x+2, self.y+2, self.width-4, self.height-4, 4, 4)

  if not self.sold then
    -- Draw Symbol

    love.graphics.setFont(gFonts.huge)
    love.graphics.setColor(1, 1, 1)
    love.graphics.printf(self.symbol, self.x+3, self.y+5, self.width-6, "center")

    -- Draw HP and DMG

    love.graphics.setFont(gFonts.big)
    love.graphics.setColor(1, .8, .8)
    love.graphics.printf(self.hp, self.x+7, self.y-4+self.height-gFonts.big:getHeight(), self.width-14, "left")

    love.graphics.setColor(1, 1, .8)
    love.graphics.printf(self.dmg, self.x+7, self.y-4+self.height-gFonts.big:getHeight(), self.width-14, "right")

    -- Draw Attributes
    
    do
      local font = gFonts.small
      love.graphics.setFont(font)
      love.graphics.setColor(1, 1, 1)

      for i, attribute in ipairs(self.attributes) do
        local details = valid_attributes[attribute]
        love.graphics.printf(details.name, self.x+3, self.y+self.height-4-gFonts.big:getHeight()+6-(4+font:getHeight())*i, self.width-6, "center")
      end
    end

    -- Draw name
    
    -- local width = gFonts.normal:getWidth(self.full_name)+14
    -- local height = gFonts.normal:getHeight()+8
    -- local x = self.x+3+(self.width-6)/2-width/2
    -- local y = self.y+self.height-4-gFonts.big:getHeight()/2-height/2

    -- love.graphics.setColor(.8, 1, 1)
    -- love.graphics.rectangle("fill", x, y, width, height, 4, 4)

    -- love.graphics.setColor(0, 0, 0)
    -- love.graphics.rectangle("fill", x+2, y+2, width-4, height-4, 4, 4)

    -- love.graphics.setColor(1, 1, 1)
    -- love.graphics.printf(self.full_name, x+7, y+4, width-14, "left")

    love.graphics.setFont(gFonts.normal)
    love.graphics.printf(self.full_name, self.x+3, self.y+self.height-4-gFonts.big:getHeight()/2-gFonts.normal:getHeight()/2, self.width-6, "center")
  else
    love.graphics.setFont(gFonts.big)
    love.graphics.setColor(1, 0, 0)
    love.graphics.printf("SOLD", self.x+3, self.y+self.height/2-gFonts.big:getHeight()/2, self.width-6, "center")
  end
end

function Card:mousepressed(x, y, button)
  assert(button ~= 1)
  --self.sold = true
  if not self.sold then
    gStateStack:push(CardInfoState(self))
  end
end

return Card
