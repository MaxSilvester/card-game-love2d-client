local utils = {}

function utils.getBase(file_path)
  return string.reverse(string.gsub(string.reverse(file_path), "^[^.]+[.]", ""))
end

return utils
