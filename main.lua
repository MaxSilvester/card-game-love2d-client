require("requires")
local StateStack = require("src.states.StateStack")
local StartState = require("src.states.game.StartState")

-- local Shop = require("src.Shop")

gServerUrl = "http://0.0.0.0:8000"

if true then
  gWidth = 1920
  gHeight = 1080
else
  gWidth = 1280
  gHeight = 720
end

function love.load()
  love.window.setMode(gWidth, gHeight, {
    fullscreen = true
  })
  gImages = {
    background = love.graphics.newImage("assets/bg.jpg")
  }
  -- shop = Shop(gWidth/4, gHeight*4/5, gWidth/2, gHeight/5)

  gStateStack = StateStack()
  gStateStack:push(StartState())

  gFonts = {
    huge = love.graphics.newFont("assets/DejaVuSans.ttf", 128*gHeight/1080),
    big = love.graphics.newFont("assets/DejaVuSans.ttf", 32*gHeight/1080),
    normal = love.graphics.newFont("assets/DejaVuSans.ttf", 18*gHeight/1080),
    small = love.graphics.newFont("assets/DejaVuSans.ttf", 15*gHeight/1080)
  }
end

function love.update(dt)
  gStateStack:update(dt)
end

function love.draw()
  gStateStack:draw()
  -- love.graphics.setFont(normal_font)
  -- love.graphics.setColor(1, 1, 1)
  -- love.graphics.draw(background, 0, 0, 0, gWidth/background:getWidth(), gHeight/background:getHeight())
  -- shop:draw()
  -- love.graphics.rectangle("fill", gWidth/4, gHeight*4/5, gWidth/2, gHeight/5)
  -- print(json.encode(shop))
  love.graphics.setFont(gFonts.small)
  love.graphics.setColor(0, 1, 0, 1)
  love.graphics.print(love.timer.getFPS().." FPS", 5, 5)
end

function love.mousepressed(x, y, button, istouch, presses)
  gStateStack:mousepressed(x, y, button)
end

function love.keypressed(key, scancode, isrepeat)
  gStateStack:keypressed(key)
end
